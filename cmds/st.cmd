#!/usr/bin/env iocsh.bash
# -----------------------------------------------------------------------------
# EPICS - DataBase
# -----------------------------------------------------------------------------
# Lakeshore 155 - Precision I/V source
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@ess.eu
# WP12 - tamas.kerenyi@ess.eu
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# EPICS modules being loaded dynamically
# -----------------------------------------------------------------------------
require(lakeshore155)

# -----------------------------------------------------------------------------
# setting parameters when not using auto deployment
# -----------------------------------------------------------------------------
epicsEnvSet("IPADDR",               "172.30.32.16")
epicsEnvSet("IPPORT",               "8888")
epicsEnvSet("LOCATION",             "E04: $(IPADDR):$(IPPORT)")
epicsEnvSet("SYS",                  "E04-SEE")
epicsEnvSet("P",                    "$(SYS):")
epicsEnvSet("DEV",                  "LS155-001")
epicsEnvSet("R",                    "$(DEV):")
epicsEnvSet("PORTNAME",             "$(SYS)-$(DEV)")
epicsEnvSet("A",                    -1)

# -----------------------------------------------------------------------------
# ESS e3 common databases
# -----------------------------------------------------------------------------
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
iocshLoad("$(lakeshore155_DIR)lakeshore155.iocsh", "P=$(P), R=$(R), IPADDR=$(IPADDR), IPPORT=$(IPPORT), PORTNAME=$(PORTNAME), A=$(A)")

iocInit()
# end-of-line required
